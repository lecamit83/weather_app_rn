import React from 'react';
import { StyleSheet, View, TextInput } from 'react-native';

function SearchBar(props) {
    return (
        <View style={styles.container}>
            <TextInput
                style={styles.textInput}
                placeholder={"SDSA"}
                />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        height: 40,
        width: 300,
        marginTop: 20,
        backgroundColor: '#666',
        marginVertical: 40,
        paddingVertical: 10,
        borderRadius: 5,
    },
    textInput : {
        flex : 1,
        color : '#fff'
    }
});

export default SearchBar;