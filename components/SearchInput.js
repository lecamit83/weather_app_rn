import React, { useState } from 'react';
import { StyleSheet, View, TextInput } from 'react-native';
import PropTypes from 'prop-types';
function SearchInput(props) {
    const [location, setLocation] = useState(props.location);
    function changeTextHandler(nLocation) {
        setLocation(nLocation);
    }
    function submitEditingHandler() {
        const { onSubmit } = props;
        if(!location) return;
        onSubmit(location);
        setLocation('');
    }
    return (
        <View style={styles.container}>
            <TextInput 
                autoCorrect={false}
                placeholderTextColor={"white"}
                placeholder={props.placeholder}
                underlineColorAndroid="transparent"
                style={styles.textInput}
                clearButtonMode={"always"}
                onChangeText={changeTextHandler}
                onSubmitEditing={submitEditingHandler}
                value={location}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 40,
        width: 300,
        marginTop: 20,
        backgroundColor: '#666',
        marginHorizontal: 40,
        paddingHorizontal: 10,
        borderRadius: 5,
    },
    textInput : {
        flex : 1,
        color : '#fff'
    }
});

SearchInput.propTypes = {
    placeholder : PropTypes.string,
    onSubmit : PropTypes.func.isRequired,
};

SearchInput.defaultProps = {
    placeholder : '...',
};

export default SearchInput;