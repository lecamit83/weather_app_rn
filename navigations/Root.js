import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import ConversationScreen from '../screens/Conversations';
import ConversationChat from '../screens/ChatConversation';
import FriendScreen from '../screens/Friends';
import ExploreScreen from '../screens/Explore';
import Header from '../components/Header';

// const conversation = createStackNavigator({
//     'ConversationScreen' : ConversationScreen,
//     'ConversationChat' : ConversationChat
// },{
//     initialRouteName : 'ConversationScreen',
//     defaultNavigationOptions: {}
// });

// const bottomTab = createBottomTabNavigator({
//     'ConversationScreen' : conversation,
//     'FriendScreen' : FriendScreen,
//     'ExploreScreen' : ExploreScreen,
// });

const HomeStack = createStackNavigator({
    Home: ConversationScreen,
    Details: ConversationChat,
});

const SettingsStack = createStackNavigator({
    Settings: ExploreScreen,
    Profile: FriendScreen,
});

const TabNavigator = createBottomTabNavigator({
    Home: HomeStack,
    Settings: SettingsStack,
});

export default createAppContainer(TabNavigator);