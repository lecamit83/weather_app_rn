/* eslint-disable global-require */
import niceImage from "../assets/weather.jpeg";

const images = {
    "nice" : niceImage,
};

export default weather => images[weather];