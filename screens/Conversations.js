import React, { } from 'react';
import { StyleSheet, View, Text, StatusBar, TouchableOpacity } from 'react-native';
import SearchBar from '../components/SearchBar';
function Conversations(props) {
    return (
        <View>
            <StatusBar  
                backgroundColor = "#b3e6ff"  
                barStyle = "dark-content"   
                hidden = {false}    
                translucent = {true}  
                />  
            <SearchBar/>
            <TouchableOpacity onPress={()=>props.navigation.navigate('ConversationChat')}>
            <Text> 
                Message Page  
            </Text>
            </TouchableOpacity>
        </View>
    );
}

export default Conversations;
