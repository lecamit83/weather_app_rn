import React, { useState } from 'react';
import { StyleSheet, View, Text, KeyboardAvoidingView, ImageBackground, ActivityIndicator, StatusBar } from 'react-native';
import { Platform } from '@unimodules/core';
import SearchInput from '../components/SearchInput';
import getImageByWeather from '../utils/getImageByWeather';

import { fetchLocationId, fetchWeather } from '../utils/api';

export default function Login(props){
    const [ location, setLocation] = useState('San Francisco');
    const [ loading , setLoading ] = useState(false);
    const [ error , setError ] = useState(false);
    const [ temperature, setTemperature ] = useState(0);




    async function updateLocationHandler(city) {
        if(!city && (city === location)) return; 
        setLoading(true);
        try {
            const locationId = await fetchLocationId(city);
            const { location , temperature } = await fetchWeather(locationId);
            setLocation(location);
            setTemperature(temperature);
            setLoading(false);
        } catch (error) {
            setLoading(false);
            setError(true);
        }
    }


    return (
        <KeyboardAvoidingView style={styles.container} behavior="padding">
            <StatusBar barStyle="light-content"/>
            <ImageBackground 
                source={getImageByWeather("nice")}
                style={styles.imageContainer}
                imageStyle={styles.image}
            >
                <View style={styles.detailsContainer}>
                    <ActivityIndicator animating={loading} color="white" size="large"/>
                    {!loading && (
                        <>
                            {error && (
                                <Text style={[styles.smallText, styles.textStyle]}>
                                Could not load weather, please try a different city.
                                </Text>
                                )
                            }
                            {!error &&(
                                <>
                                <Text style={[styles.textStyle, styles.largeText]}>{location}</Text>
                                <Text style={[styles.textStyle, styles.smallText]}>Light Cloud</Text>
                                <Text style={[styles.textStyle, styles.largeText]}>{temperature}°</Text>
                                </>
                            )}
                            <SearchInput 
                                placeholder="Entering city name here ..."
                                onSubmit={updateLocationHandler}
                                value={updateLocationHandler}
                                />
                        </>
                    )}
                </View>
            </ImageBackground>
        </KeyboardAvoidingView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#34495E',
    },
    detailsContainer : {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.2)',
        paddingHorizontal: 20,
    },
    textStyle : {
        textAlign: 'center',
        fontFamily : Platform.OS === 'ios' ? 'AvenirNext-Regular' : 'Roboto',
    },
    largeText : {
        fontSize: 44,
    },
    smallText: {
        fontSize : 18,
    },
    imageContainer : {
        flex : 1,
        justifyContent : 'center',
        alignItems: 'center',
    },
    image : {
        flex : 1,
        height : null,
        width : null,
        resizeMode : "cover",
    },
});